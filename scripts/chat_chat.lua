-- This file is provided under the Open Game License version 1.0a
-- For more information on OGL and related issues, see 
--   http://www.wizards.com/d20

-- See the license.html file included with this distribution for
-- conditions of use.

-- All producers of work derived from this definition are adviced to
-- familiarize themselves with the above licenses, and to take special
-- care in providing the definition of Product Identity (as specified
-- by the OGL) in their products.

function applyModifierStackToRoll(draginfo)
  if ModifierStack.isEmpty() then
    --[[ do nothing ]]
  elseif draginfo.getNumberData() == 0 and draginfo.getDescription() == "" then
    draginfo.setDescription(ModifierStack.getDescription());
    draginfo.setNumberData(ModifierStack.getSum());
  else
    local originalnumber = draginfo.getNumberData();
    local numstr = tostring(originalnumber);
    if originalnumber > 0 then
      numstr = "+" .. originalnumber;
    end

    local moddesc = ModifierStack.getDescription(true);
    local desc = draginfo.getDescription();
    
    if numstr ~= "0" then
      desc = desc .. " " .. numstr;
    end
    if moddesc ~= "" then
      desc = desc .. " (" .. moddesc .. ")";
    end
    
    draginfo.setDescription(desc);
    draginfo.setNumberData(draginfo.getNumberData() + ModifierStack.getSum());
  end
  
  ModifierStack.reset();
end

function onDiceLanded(draginfo)
  if ChatManager.getDieRevealFlag() then
    draginfo.revealDice(true);
  end

  if draginfo.isType("fullattack") then
    for i = 1, draginfo.getSlotCount() do
      draginfo.setSlot(i);

      if not ModifierStack.isEmpty() then
        local originalnumber = draginfo.getNumberData();
        local numstr = originalnumber;
        if originalnumber > 0 then
          numstr = "+" .. originalnumber;
        end
        
        draginfo.setStringData(draginfo.getStringData() .. " " .. numstr .. " (" .. ModifierStack.getDescription(true) .. ")");
        draginfo.setNumberData(draginfo.getNumberData() + ModifierStack.getSum());
      end

      deliverDieResults(draginfo);
    end
    
    ModifierStack.reset();
    return true;
  elseif draginfo.isType("dice") then
    applyModifierStackToRoll(draginfo);
    deliverDieResults(draginfo);
    return true;
  end
end

function deliverDieResults(draginfo)
  local entry = {};
  local dieList = draginfo.getDieList();
  entry.text = draginfo.getDescription();
  entry.font = "systemfont";
  entry.diemodifier = draginfo.getNumberData();

  if dieList and #dieList==2 then
    local d10Found = false;
    local d100Found = false;
    local d100Result = 0;
    for k,die in pairs(dieList) do
      if die.type == "d100" then
        d100Found = true;  
        d100Result = d100Result + tonumber(die.result)
      end
      if die.type == "d10" then
        d10Found = true;
        d100Result = d100Result + tonumber(die.result)
      end
    end
    if d100Found and d10Found then
      -- yep, this is a d100 roll
      dieList = {{type="d100",result=d100Result}};
    end
  end

  entry.dice = dieList;
  
  if User.isHost() then
    if ChatManager.getDieRevealFlag() then
      entry.dicesecret = false;
    end
    entry.sender = GmIdentityManager.getCurrent();
  else
    entry.sender = User.getIdentityLabel();
  end
  
  -- add an optional total to the end of the message
  if PreferenceManager.load(Preferences.ShowTotals.PrefName)==Preferences.Yes then
    local total = entry.diemodifier;
    for k,die in ipairs(entry.dice) do
      if die.result then
        total = total + die.result;
      end
    end
    if total~= 0 then
      local text = entry.text or "";
      if text=="" then
        entry.text = "["..total.."]";
      else
        entry.text = text.." ["..total.."]";
      end
    end
  end
  
  entry = ChatManager.checkPortrait(entry,"chat");
  deliverMessage(entry);
end


function onDrop(x, y, draginfo)
  if draginfo.getType() == "number" then
    applyModifierStackToRoll(draginfo);
  end
end

function moduleActivationRequested(module)
  local msg = {};
  msg.text = "Players have requested permission to load '" .. module .. "'";
  msg.font = "systemfont";
  msg.icon = "indicator_moduleloaded";
  addMessage(msg);
end

function moduleUnloadedReference(module)
  local msg = {};
  msg.text = "Could not open sheet with data from unloaded module '" .. module .. "'";
  msg.font = "systemfont";
  addMessage(msg);
end

function onReceiveMessage(msg)
  if ChatManager and ChatManager.executeCommand then
    return ChatManager.executeCommand(msg);
  end
end

function onInit()
  ChatManager.registerControl(self);
  
  if User.isHost() then
    Module.onActivationRequested = moduleActivationRequested;
  end

  Module.onUnloadedReference = moduleUnloadedReference;
end

function onClose()
  ChatManager.registerControl(nil);
end
