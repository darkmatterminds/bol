-- This file is provided under the Open Game License version 1.0a
-- For more information on OGL and related issues, see 
--   http://www.wizards.com/d20

-- See the license.html file included with this distribution for
-- conditions of use.

-- All producers of work derived from this definition are adviced to
-- familiarize themselves with the above licenses, and to take special
-- care in providing the definition of Product Identity (as specified
-- by the OGL) in their products.

function getCompletion(str)
	--[[ Find a matching completion for the given string ]]
  local list = {};
  local fieldname = nil;
  if lookupFieldName and lookupFieldName[1]~="" then
    fieldname = lookupFieldName[1];
  end
  if lookup and type(lookup[1])=="string" then
    for n=1,#lookup do
      local name = lookup[n];
      local thislist = Global.Lookups[name];
      if thislist then
        for _key,_value in pairs(thislist) do
          list[_key] = _value;
        end
      end
    end
  end
	if list then
		for _key, _value in pairs(list) do
			if fieldname then
		    -- If a lookup field name is present, use it to index the lookup entry
				if string.lower(str) == string.lower(string.sub(_value[fieldname], 1, #str)) then
					return string.sub(_value[fieldname], #str + 1);
				end
			else							
			  -- If there is no fieldname, either use the key (if it is a string) or the value itself
			  local value;
			  if type(_key)=="string" then
			    value = _key;
			  else
			    value = _value or "";
			  end
				if string.lower(str) == string.lower(string.sub(value, 1, #str)) then
					return string.sub(value, #str + 1);
				end
			end			
		end
	end
	return "";
end

function onChar()
	--[[ When a new character is appended to a class label, autocomplete it if a match is found ]]
	local str = getValue();
	local pos = getCursorPosition();

	if pos == (#str + 1) then
		completion = getCompletion(str);

		if completion ~= "" then
			value = getValue();
			newvalue = string.sub(value, 1, pos-1) .. completion .. string.sub(value, pos);

			setValue(newvalue);
			setSelectionPosition(pos + #completion);
		end

		return;
	end
end

