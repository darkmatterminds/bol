-- This file is provided under the Open Game License version 1.0a
-- For more information on OGL and related issues, see 
--   http://www.wizards.com/d20

-- See the license.html file included with this distribution for
-- conditions of use.

-- All producers of work derived from this definition are adviced to
-- familiarize themselves with the above licenses, and to take special
-- care in providing the definition of Product Identity (as specified
-- by the OGL) in their products.

localdatabasenode = nil;
id = "";

function setId(n)
  id = n;
end

function setLocalNode(node)
  localdatabasenode = node;
  
  if node then
    if node.isStatic() then
      campaign.setValue("Server character");
    else
      campaign.setValue("Local character");
      registerMenuItem("Delete character", "delete", 3);
      
      portrait.setVisible(false);
      localportrait.setVisible(true);

      local portraitfile = User.getLocalIdentityPortrait(node);
      if portraitfile then
        localportrait.setFile(portraitfile);
      end
    end
  else
    campaign.setValue("Server character");
  end
end

function requestResponse(result, identity)
  if result and identity then
    local colortable = {};
    if CampaignRegistry and CampaignRegistry.colortables and CampaignRegistry.colortables[identity] then
      colortable = CampaignRegistry.colortables[identity];
    end
    
    User.setCurrentIdentityColors(colortable.color or "000000", colortable.blacktext or false);

    windowlist.window.close();
  else
    error.setVisible(true);
  end
end

function onMenuSelection(item)
  if localdatabasenode and item == 3 then
    localdatabasenode.delete();
    close();
  end
end
