-- This file is provided under the Open Game License version 1.0a
-- For more information on OGL and related issues, see 
--   http://www.wizards.com/d20

-- See the license.html file included with this distribution for
-- conditions of use.

-- All producers of work derived from this definition are adviced to
-- familiarize themselves with the above licenses, and to take special
-- care in providing the definition of Product Identity (as specified
-- by the OGL) in their products.

local tabIndex = 1;
local tabs = {};

function clear()
  for i=1,#tabs do
    local tab = tabs[i];
    if tab.widget then
      tab.widget.destroy();
    end
    if window[tab.subwindow] then
      window[tab.subwindow].setVisible(false);
    end
    if window[tab.scroller] then
      window[tab.scroller].setVisible(false);
    end
  end
  tabs = {};
  update();
end

function addTab(subwindow,icon,scroller)
  local index = #tabs + 1;
  local newtab = {};
  newtab.subwindow = subwindow;
  newtab.icon = icon;
  newtab.scroller = scroller;
  if icon then
    newtab.widget = addBitmapWidget(icon);
  end
  tabs[index] = newtab;
  update();
end

function setTab(index,subwindow,icon,scroller)
  local tab = tabs[index];
  if tab.widget then
    tab.widget.destroy();
  end
  if window[tab.subwindow] then
    window[tab.subwindow].setVisible(false);
  end
  if window[tab.scroller] then
    window[tab.scroller].setVisible(false);
  end
  tab = {};
  tab.subwindow = subwindow;
  tab.icon = icon;
  tab.scroller = scroller;
  if icon then
    tab.widget = addBitmapWidget(icon);
  end
  tabs[index] = tab;
  update();
  activateTab(tabIndex);
end

function activateTab(index)
  local thistab;
  index = tonumber(index);
  if index<1 or index>#tabs then
    return;
  end
  
  -- Hide active tab, fade text labels
  thistab = tabs[tabIndex]
  if thistab.widget then
    thistab.widget.setColor("80ffffff");
  end
  if window[thistab.subwindow] then
    window[thistab.subwindow].setVisible(false);
  end
  if window[thistab.scroller] then
    window[thistab.scroller].setVisible(false);
  end

  -- Set new index
  tabIndex = index;
  thistab = tabs[tabIndex];

  -- Move helper graphic into position
  topWidget.setPosition("topleft", 5, 67*(tabIndex-1)+7);
  if tabIndex == 1 then
    topWidget.setVisible(false);
  else
    topWidget.setVisible(true);
  end
  
  -- Activate text label and subwindow
  if thistab.widget then
    thistab.widget.setColor("ffffffff");
  end
  if window[thistab.subwindow] then
    window[thistab.subwindow].setVisible(true);
  end
  if window[thistab.scroller] then
    window[thistab.scroller].setVisible(true);
  end
  
  -- Call the onActivate handler, if present
  if self.onActivate then
    self.onActivate(index);
  end
  
  -- Done
end

function onClickDown(button, x, y)
  local i = math.ceil(y/67);
  -- Make sure index is in range and activate selected
  if i > 0 and i < #tabs+1 then
    activateTab(i);
  end
end

function onDoubleClick(x, y)
  -- Emulate single click
  onClickDown(1, x, y);
end

function update()
  -- show/hide
  setVisible(#tabs>0);
  -- set the control height
  local h = #tabs * 67 + 22;
  setAnchoredHeight(h);
  -- position the tab icons 
  for n, tab in ipairs(tabs) do
    if tab.widget then
      tab.widget.setPosition("topleft", 7, 67*(n-1)+41);
      tab.widget.setColor("80ffffff");
    end
  end
end

function onInit()
  -- Create a helper graphic widget to indicate that the selected tab is on top
  topWidget = addBitmapWidget("tabtop");
  topWidget.setVisible(false);
  -- add all the static tabs
  if tab and tab[1] then
    for n, v in ipairs(tab) do
      if type(v)~="boolean" then
        local subwindow,icon,scroller;
        if v.subwindow and v.subwindow[1] then
          subwindow = v.subwindow[1];
        end
        if v.icon and v.icon[1] then
          icon = v.icon[1];
        end
        if v.scroller and v.scroller[1] then
          scroller = v.scroller[1];
        end
        addTab(subwindow,icon,scroller);
      end
    end
  end
  -- refresh the layout
  update();
  -- set the active tab, if there is one
  if activate then
    activateTab(activate[1]);
  end
end
