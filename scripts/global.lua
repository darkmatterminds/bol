-- This file is provided under the Open Game License version 1.0a
-- For more information on OGL and related issues, see 
--   http://www.wizards.com/d20

-- See the license.html file included with this distribution for
-- conditions of use.

-- All producers of work derived from this definition are adviced to
-- familiarize themselves with the above licenses, and to take special
-- care in providing the definition of Product Identity (as specified
-- by the OGL) in their products.

-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--
-- The Global module allows code segments to be re-used between different parts of the ruleset.
--

-- ##################### External Functions and Variables - can be called by other code #########################################

Lookups = {};

-- CopyNode copies the source node and all its children, creating a new child under the target node
-- It returns the newly created node.  If the rename parameter is set, a new name is created for complex nodes.

function CopyNode(source, target, rename)
  local newnode;
  local nodeType = source.getType();
  if not target or target.getType()~="node" then
    return nil;
  end
  if nodeType=="number" or nodeType=="string" or nodeType=="image" or nodeType=="dice" or nodeType=="windowreference" then
    newnode = target.createChild(source.getName(),nodeType);
    newnode.setValue(source.getValue());
    return newnode;
  end
  -- copying formattedtext isn't supported
  if nodeType=="formattedtext" then
    -- don't bother trying to create a formatted text node
    return nil;
  end
  -- unknown value?
  if nodeType~="node" then
    -- don't create anything. Should perhaps throw an error
    return nil;
  end
  -- must be a complex node, we need to repeat the process on all child nodes.
  if rename then
    newnode = target.createChild();
  else
    newnode = target.createChild(source.getName());
  end
  for k,subnode in pairs(source.getChildren()) do
    CopyNode(subnode,newnode);
  end
  return newnode;
  -- done
end

-- ##################### Internal Functions - cannot be called from outside the module ############################

function onInit()
  Lookups.Gender = {["Male"]={Example="Man"},["Female"]={Example="Woman"}};
end