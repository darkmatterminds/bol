-- This file is provided under the Open Game License version 1.0a
-- For more information on OGL and related issues, see 
--   http://www.wizards.com/d20

-- See the license.html file included with this distribution for
-- conditions of use.

-- All producers of work derived from this definition are adviced to
-- familiarize themselves with the above licenses, and to take special
-- care in providing the definition of Product Identity (as specified
-- by the OGL) in their products.

local scriptName = "referencetable.lua";
local maxHeight;
local minHeight;
local maxWidth;
local minWidth;
local defaultX;
local defaultY;
local defaultWidth;
local defaultHeight;
local resizeVertical = false;
local resizeHorizontal = false;
local selfcallResize = false;
local radialResetMenuPosition;
local radialResetMenuIcon;
local radialResetMenuLabel = "Restore to default size and location.";

function onInit()
  local functionName = "onInit";
  local dbNode = getDatabaseNode();
  if super and super.onInit then
    super.onInit();
  end
  -- check for a saved position/height in the registry and don't set here if there is one (because it will just override these anyway)
  if dbNode and dbNode.getChild("placement") then
    if dbNode.getChild("placement.position") then
      if dbNode.getChild("placement.position.x").getValue() and tonumber(dbNode.getChild("placement.position.x").getValue()) then
        defaultX = tonumber(dbNode.getChild("placement.position.x").getValue());
      end
      if dbNode.getChild("placement.position.y").getValue() and tonumber(dbNode.getChild("placement.position.y").getValue()) then
        defaultY = tonumber(dbNode.getChild("placement.position.y").getValue());
      end
      if defaultX and defaultY then
        setPosition(defaultX, defaultY);
      end
    end
    if dbNode.getChild("placement.size") then
      if dbNode.getChild("placement.size.height").getValue() and tonumber(dbNode.getChild("placement.size.height").getValue()) then
        defaultHeight = tonumber(dbNode.getChild("placement.size.height").getValue());
      end
      if dbNode.getChild("placement.size.width").getValue() and tonumber(dbNode.getChild("placement.size.width").getValue()) then
        defaultWidth = tonumber(dbNode.getChild("placement.size.width").getValue());
      end
      if defaultWidth and defaultHeight then
        setSize(defaultWidth, defaultHeight);
      end
    end
  end
  if dbNode and dbNode.getChild("resetmenuitem") then
    if dbNode.getChild("resetmenuitem.position") and dbNode.getChild("resetmenuitem.position").getValue()
       and tonumber(dbNode.getChild("resetmenuitem.position").getValue()) then
      radialResetMenuPosition = tonumber(dbNode.getChild("resetmenuitem.position").getValue());
    end 
    if dbNode.getChild("resetmenuitem.icon") and dbNode.getChild("resetmenuitem.icon").getValue()
       and dbNode.getChild("resetmenuitem.icon").getValue() ~= "" then
      radialResetMenuIcon = dbNode.getChild("resetmenuitem.icon").getValue();
    end 
    if dbNode.getChild("resetmenuitem.label") and dbNode.getChild("resetmenuitem.label").getValue() then
      radialResetMenuLabel = dbNode.getChild("resetmenuitem.label").getValue();
    end 
    if radialResetMenuPosition and radialResetMenuIcon then
      registerMenuItem(radialResetMenuLabel, radialResetMenuIcon, radialResetMenuPosition)
    end
  end
  if dbNode and dbNode.getChild("sizelimits") then
    if dbNode.getChild("sizelimits.maximum") then
      if dbNode.getChild("sizelimits.maximum.height").getValue() and tonumber(dbNode.getChild("sizelimits.maximum.height").getValue()) then
        maxHeight = tonumber(dbNode.getChild("sizelimits.maximum.height").getValue());
      end
      if dbNode.getChild("sizelimits.maximum.width").getValue() and tonumber(dbNode.getChild("sizelimits.maximum.width").getValue()) then
        maxWidth = tonumber(dbNode.getChild("sizelimits.maximum.width").getValue());
      end
    end
    if dbNode.getChild("sizelimits.minimum") then
      if dbNode.getChild("sizelimits.minimum.height") and dbNode.getChild("sizelimits.minimum.height").getValue()
         and tonumber(dbNode.getChild("sizelimits.minimum.height").getValue()) then
        minHeight = tonumber(dbNode.getChild("sizelimits.minimum.height").getValue());
      end
      if dbNode.getChild("sizelimits.minimum.width") and dbNode.getChild("sizelimits.minimum.width").getValue()
         and tonumber(dbNode.getChild("sizelimits.minimum.width").getValue()) then
        minWidth = tonumber(dbNode.getChild("sizelimits.minimum.width").getValue());
      end
    end
    if dbNode.getChild("sizelimits.dynamic") then
      if dbNode.getChild("sizelimits.dynamic").getValue() and (dbNode.getChild("sizelimits.dynamic").getValue() == "horizontal" 
         or dbNode.getChild("sizelimits.dynamic").getValue() == "both") then
        resizeHorizontal = true;
      end
      if dbNode.getChild("sizelimits.dynamic").getValue() and (dbNode.getChild("sizelimits.dynamic").getValue() == "vertical" 
         or dbNode.getChild("sizelimits.dynamic").getValue() == "both") then
        resizeVertical = true;
      end
    end
  end
  self.onSizeChanged = sizeChanged;
end

function sizeChanged(source)
  if selfcallResize == false then
  
    local curWidth, curHeight = getSize();
    local newWidth = curWidth;
    local newHeight = curHeight;
    if resizeVertical == false 
    and defaultHeight then
      newHeight = defaultHeight;
    else
      if maxHeight
      and curHeight > maxHeight then
        newHeight = maxHeight;
      end
      if minHeight
      and minHeight > curHeight then
        newHeight = minHeight;
      end
    end
    
    if resizeHorizontal == false 
    and defaultWidth then
      newWidth = defaultWidth;
    else
      if maxWidth
      and curWidth > maxWidth then
        newWidth = maxWidth;
      end
      if minWidth
      and minWidth > curWidth then
        newWidth = minWidth;
      end
    end
    
    if (   newWidth ~= curWidth
      or newHeight ~= curHeight) then
      selfcallResize = true;
      setSize(newWidth, newHeight);
    end
  end
  selfcallResize = false;
  
  return true;
end 

function onMenuSelection(slot1)
  if radialResetMenuPosition
  and radialResetMenuPosition == slot1 then
  
    if defaultWidth
    and defaultHeight then
      selfcallResize = true;
      setSize(defaultWidth, defaultHeight);  
    end
    
    if defaultX
    and defaultY then
      setPosition(defaultX, defaultY);
    end
    
    return true;
  end
end