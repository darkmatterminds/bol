-- This file is provided under the Open Game License version 1.0a
-- For more information on OGL and related issues, see 
--   http://www.wizards.com/d20

-- See the license.html file included with this distribution for
-- conditions of use.

-- All producers of work derived from this definition are adviced to
-- familiarize themselves with the above licenses, and to take special
-- care in providing the definition of Product Identity (as specified
-- by the OGL) in their products.

local portraitwidget = nil;

function onInit()
  if super and super.onInit then
    super.onInit();
  end
  if window.getDatabaseNode() and window.getDatabaseNode().getName() then
    if getName() == "FullPortraitFrame" then
      -- full-sized portrait
      portraitwidget = window.FullPortraitFrame.addBitmapWidget("portrait_" .. window.getDatabaseNode().getName().. "_charlist");
    end
    if getName() == "SmallPortraitFrame" then
      -- small portrait
      portraitwidget = window.SmallPortraitFrame.addBitmapWidget("portrait_" .. window.getDatabaseNode().getName().. "_token");
    end
  end
  if portraitwidget and portraitwidget.getBitmap() and window.logo then
    window.logo.setVisible(false);
  end  
end

function onDrag(button, x, y, dragdata)
  local base = nil;
  local identityname = window.getDatabaseNode().getName();

  dragdata.setType("playercharacter");
  dragdata.setTokenData("portrait_" .. identityname .. "_token");
  dragdata.setDatabaseNode("charsheet." .. identityname);
  dragdata.setStringData(window.name.getValue());
  
  base = dragdata.createBaseData();
  base.setType("token");
  base.setTokenData("portrait_" .. identityname .. "_token");

  return true;
end
