-- This file is provided under the Open Game License version 1.0a
-- For more information on OGL and related issues, see 
--   http://www.wizards.com/d20

-- See the license.html file included with this distribution for
-- conditions of use.

-- All producers of work derived from this definition are adviced to
-- familiarize themselves with the above licenses, and to take special
-- care in providing the definition of Product Identity (as specified
-- by the OGL) in their products.

function onInit()
  if User.isLocal() then
    DesktopManager.registerStackShortcut("button_characters", "button_characters_down", "Characters", "identityselection");
    DesktopManager.registerStackShortcut("button_pointer", "button_pointer_down", "Colors", "pointerselection");
    DesktopManager.registerStackShortcut("button_portraits", "button_portraits_down", "Portraits", "portraitselection");
    DesktopManager.registerStackShortcut("button_modules", "button_modules_down", "Modules", "moduleselection");
    DesktopManager.registerStackShortcut("button_prefs", "button_prefs_down", "Preferences", "preflist");

    DesktopManager.registerDockShortcut("button_library", "button_library_down", "Library", "library");
  else
    if User.isHost() then
      -- GM-specific stack items
      DesktopManager.registerStackShortcut("button_light", "button_light_down", "Lighting", "lightingselection");
      DesktopManager.registerStackShortcut("button_pointer", "button_pointer_down", "Colors", "pointerselection");
      DesktopManager.registerStackShortcut("button_tracker", "button_tracker_down", "Combat tracker", "combattracker", "combattracker");
      DesktopManager.registerStackShortcut("button_characters", "button_characters_down", "Characters", "charactersheetlist", "charsheet");
      -- GM-specific dock items
      DesktopManager.registerDockShortcut("button_book", "button_book_down", "Story", "storylist", "story");
      DesktopManager.registerDockShortcut("button_encounters", "button_encounters_down", "Encounters", "encounterlist", "encounter");
      DesktopManager.registerDockShortcut("button_maps", "button_maps_down", "Maps & Images", "imagelist", "image");
      DesktopManager.registerDockShortcut("button_people", "button_people_down", "Personalities", "npclist", "npc");
      DesktopManager.registerDockShortcut("button_itemchest", "button_itemchest_down", "Items", "itemlist", "item");
    else
      -- Player-specific stack items
      DesktopManager.registerStackShortcut("button_portraits", "button_portraits_down", "Portraits", "portraitselection");
      DesktopManager.registerStackShortcut("button_pointer", "button_pointer_down", "Colors", "pointerselection");
      DesktopManager.registerStackShortcut("button_characters", "button_characters_down", "Characters", "identityselection");
    end
    -- Generic stack items
    DesktopManager.registerStackShortcut("button_modules", "button_modules_down", "Modules", "moduleselection");
    DesktopManager.registerStackShortcut("button_prefs", "button_prefs_down", "Preferences", "preflist");
    DesktopManager.registerStackShortcut("button_modifiers", "button_modifiers_down", "Modifiers", "modifierlist", "modifier");
    DesktopManager.registerStackShortcut("button_effects", "button_effects_down", "Effects", "effectlist", "effect");
    -- Generic dock items
		DesktopManager.registerDockShortcut("button_notes", "button_notes_down", "Notes", "notelist", "note");
    DesktopManager.registerDockShortcut("button_library", "button_library_down", "Library", "library");
    -- Token box
    DesktopManager.registerDockShortcut("button_tokencase", "button_tokencase_down", "Tokens", "tokenbag", nil, true);
  end
end