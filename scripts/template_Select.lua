-- This file is provided under the Open Game License version 1.0a
-- For more information on OGL and related issues, see 
--   http://www.wizards.com/d20

-- See the license.html file included with this distribution for
-- conditions of use.

-- All producers of work derived from this definition are adviced to
-- familiarize themselves with the above licenses, and to take special
-- care in providing the definition of Product Identity (as specified
-- by the OGL) in their products.

local readonlyvalue = false;
local sourcenodename = "";
local source = nil;

local selecteditem = nil;
local rowheight = 15;
local fontvalue = "";
local selectedfont = "";
local framevalue = "";
local selectedframe = "";
local scrollbar = nil;

function onInit()
  if super and super.onInit then
    super.onInit();
  end
  if not sourceless and window.getDatabaseNode() then
    -- Get value from source node
    if sourcename then
      sourcenodename = sourcename[1];
    else
      sourcenodename = getName();
    end
    window.getDatabaseNode().onChildUpdate = capture;
    capture();
  end
  if readonly or (window.getDatabaseNode() and window.getDatabaseNode().isStatic()) then
    setReadOnly(true);
  end
  if getName()~="" then
    scrollbar = window.createControl("Scrollbar","");
    scrollbar.setTarget(getName());
  end
  self.onScroll = initcheck;
  setRows(tonumber(size[1]) or 0);
  setFonts(fonts[1].normal[1],fonts[1].selected[1]);
  setFrames(frames[1].normal[1],frames[1].selected[1]);
  applySort();
end

function getFonts()
  return fontvalue,selectedfont;
end

function setFonts(normal,sel)
  if type(normal)=="boolean" then normal = "" end;
  if type(sel)=="boolean" then sel = "" end;
  fontvalue = normal or "";
  selectedfont = sel or "";
  for i,opt in ipairs(getWindows()) do
    opt.setFonts(fontvalue,selectedfont);
  end
end

function getFrames()
  return framevalue,selectedframe;
end

function setFrames(normal,sel)
  if type(normal)=="boolean" then normal = "" end;
  if type(sel)=="boolean" then sel = "" end;
  framevalue = normal or "";
  selectedframe = sel or "";
  for i,opt in ipairs(getWindows()) do
    opt.setFrames(framevalue,selectedframe);
  end
end

function scrollToItem(value)
  local row=0;
  local sx,px,vx,sy,py,vy;
  local v,p;
  for i,opt in ipairs(getWindows()) do
    if opt.Value.getValue()==value then
      row = i;
    end
  end
  if row==0 then
    return;
  end
  row = row - 1;
  sx,px,vx,sy,py,vy = getScrollState();
  v = math.floor(vy/rowheight);
  p = math.ceil(py/rowheight);
  if row<p then
    --[[ scroll so row is at the top ]]
    p = row;
  elseif row>=(p+v) then
    --[[ scroll so row is at the bottom ]]
    p = (row - v) + 1;
    if p<0 then p=0 end;
  else
    --[[ nothing to do ]]
    return;
  end
  setScrollPosition(0,p*rowheight);
end

function setRows(num)
  if num==0 then
    num = #(getWindows());
  end
  setHeight(num*rowheight);
end

function getRows()
  local w,h = getSize();
  return math.floor(h/rowheight);
end

function setValue(value)
  local opt = nil;
  for i,win in ipairs(getWindows()) do
    if win.Value.getValue()==value then
      opt = win;
    end
  end
  selectitem(opt);
end

function getValue()
  if selecteditem then
    return selecteditem.Value.getValue();
  else
    return "";
  end
end

function getText()
  if selecteditem then
    return selecteditem.Text.getValue();
  else
    return "";
  end
end

function add(value, text)
  if type(value)=="table" then
    text  = value.Text;
    value = value.Value;
  end
  if type(value)~="string" then value = text end;
  if type(text)~="string" then text = value end;
  if type(value)=="string" and type(text)=="string" then
    local opt = createWindow();
    opt.Text.setValue(text);
    opt.Value.setValue(value);
    opt.setFonts(fontvalue,selectedfont);
    opt.setFrames(framevalue,selectedframe);
    if readonlyvalue then
      opt.setReadOnly(true);
    end
    return opt;
  end
  return nil;
end

function getOptions()
  local list = {};
  for i,win in ipairs(getWindows()) do
    local text = win.Text.getValue();
    local value = win.Value.getValue();
    local opt = {Text=text, Value=value};
    table.insert(list,opt);
  end
  return list;
end

function clearOptions()
  closeAll();
  if selecteditem then
    selecteditem = nil;
    if self.onSelectionChanged then
      self.onSelectionChanged();
    end
  end
end

function addOptions(list)
  for i,opt in ipairs(list) do
    add(opt);
  end
end

function optionClicked(opt)
  if opt and opt.setSelected then
    selectitem(opt);
  end
end

function onSortCompare(opt1,opt2)
  return opt1.Value.getValue()>opt2.Value.getValue();
end

function selectitem(opt)
  if selecteditem==opt then
    return;
  end
  if selecteditem then
    selecteditem.setSelected(false);
  end
  applySort();
  if opt then
    opt.setSelected(true);
  end
  selecteditem = opt;
  if source then
    if source.getValue()~=getValue() then
      source.setValue(getValue());
    end
  end
  if self.onSelectionChanged then
    self.onSelectionChanged();
  end
end

function setHeight(height)
  if bounds and bounds[1] then
    local w,h = getSize();
    local x,y = getPosition();
    setStaticBounds(x,y,w,height);
  else
    setAnchoredHeight(height);
  end
end

function isReadOnly()
  return readonlyvalue;
end

function setReadOnly(state)
  if getDatabaseNode() and getDatabaseNode().isStatic() then
    return;
  end
  if state then
    readonlyvalue = true;
  else
    readonlyvalue = false;
  end
  for i,opt in ipairs(getWindows()) do
    opt.setReadOnly(readonlyvalue);
  end
end

function initcheck()
  local sx,px,vx,sy,py,vy = getScrollState();
  if sx==0 and sy==0 then
    return;
  end
  self.onScroll = function () end;
  nodeChanged();
  if self.afterInit then
    self.afterInit();
  end
end

function capture()
  source = window.getDatabaseNode().createChild(sourcenodename, "string");
  if source then
    window.getDatabaseNode().onChildUpdate = function () end;
    source.onUpdate = nodeChanged;
    nodeChanged();
  end
end

function nodeChanged()
  if source then
    local value = source.getValue();
    if getValue()~=value then
      setValue(value);
    end
  end
end
